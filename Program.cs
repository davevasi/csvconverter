﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.VisualBasic.FileIO;


namespace CsvConverter
{
    static class Program
    {

        static void Main(string[] args)
        {
            string delimiter = null;
            string outputName = null;
            List<string[]> strColArry = new List<string[]>();
            List<string[]> rowData = new List<string[]>();


            var file = GetFile();
            var fileType = TorCKey();
            var cntofColumns = GetCountofCol();

            if (fileType == "t")
            {
                delimiter = @"\t";
               
            }
            else
            {
                delimiter = ",";
               
            }

            using (TextFieldParser csvReader = new TextFieldParser(file))
            {
                csvReader.SetDelimiters(new string[] { delimiter });
                csvReader.HasFieldsEnclosedInQuotes = false;
                string[] colFields = csvReader.ReadFields();
                //strColArry.Add(colFields.ToArray());

                while (!csvReader.EndOfData)
                {
                    string[] fieldData = csvReader.ReadFields();

                    if (cntofColumns > fieldData.Count())
                    {
                        cntofColumns = fieldData.Count();
                    }

                    rowData.Add(fieldData);

                    //for (int i = 0; i < cntofColumns; i++)
                    //{
                    //    if (i != 0)
                    //    {
                    //        if (fieldData[i] == "")
                    //        {
                    //            fieldData[i] = null;
                    //        }


                            
                    //    }
                    //}

                }

                if (rowData.Count() > 0 && IsCorrectFile(colFields.Count(), cntofColumns))
                {
                    if (cntofColumns > 0 && cntofColumns <= colFields.Count())
                    {
                        outputName =  "CorrectFile.csv";
                        using (var wrt = new StreamWriter(ToApplicationPath(outputName)))
                        {
                           
                            foreach (var itm in rowData)
                            {

                                wrt.WriteLine(string.Join(delimiter, itm.Take(cntofColumns)));

                            }
                            wrt.Flush();

                        }

                    }
                }
                

                if(rowData.Count() > 0 && !IsCorrectFile(colFields.Count(),cntofColumns))
                {
                    if (cntofColumns > 0 && cntofColumns <= colFields.Count())
                    {
                        outputName =  "IncorrectFile.csv";
                        using (var wrt = new StreamWriter(ToApplicationPath(outputName)))
                        {
                            foreach (var itm in rowData)
                            {

                                wrt.WriteLine(string.Join(delimiter, itm.Take(cntofColumns)));

                            }
                            wrt.Flush();

                        }

                    }
                }
            }

        }

        private static bool IsCorrectFile(int count, int cntofColumns)
        {
            try
            {
                if (count == cntofColumns)
                    return true;
            }
            catch (Exception err)
            {

                throw;
            }
            return false;
        }

        private static int GetCountofCol()
        {
            try
            {
                Console.WriteLine("How many fields should each record contain?");
                var cntCol = 0;
               

                    bool isInt = false;
                    while (!isInt)
                    {
                        var cntofCol = Console.ReadLine();
                        if (Int32.TryParse(cntofCol, out int cnt))
                        {
                             cntCol = cnt;
                            isInt = true;
                        }
                        else
                        {
                            Console.WriteLine("Please input a valid number.");

                        }
                    }
                return cntCol;

            }
            catch (Exception err)
            {

                throw;
            }
        }

        private static string GetFile()
        {
            Console.WriteLine("Where is the file located?");
            var fileRsp = Console.ReadLine();
            if (!isValidPath(fileRsp))
            {
                Console.WriteLine("The file doesn't exist, Please try again");
                Restart();
            }

            return fileRsp;
        }

        private static bool isValidPath(string fileRsp)
        {
            try
            {
                if (fileRsp == null || fileRsp == "")
                    return false;

                if (File.Exists(fileRsp))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {

                throw;
            }
        }


        private static string TorCKey()
        {
            string pressValue=String.Empty;
            try
            {
                bool istorc = false;
                while (!istorc)
                {
                    Console.WriteLine("Please input c for CSV or t for TSV.");
                    var pressedKey = Console.ReadKey().KeyChar;
                    if ((pressedKey == 't') || (pressedKey == 'c'))
                    {
                        istorc = true;
                        pressValue =  pressedKey.ToString();
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine("Please input c for CSV or t for TSV.");
                        
                    }
                }
                return pressValue;
            }
            catch (Exception err)
            {

                throw;
            }
        }

        private static void Restart()
        {
            System.Diagnostics.Process.Start(AppDomain.CurrentDomain.BaseDirectory + "\\CsvConverter.exe");

            // Closes the current process
            Environment.Exit(0);
        }



        public static string ToApplicationPath(this string fileName)
        {
            var exePath = Path.GetDirectoryName(System.Reflection
                                .Assembly.GetExecutingAssembly().CodeBase);
            Regex appPathMatcher = new Regex(@"(?<!fil)[A-Za-z]:\\+[\S\s]*?(?=\\+bin)");
            var appRoot = appPathMatcher.Match(exePath).Value;
            return Path.Combine(appRoot, fileName);
        }

    }
}
